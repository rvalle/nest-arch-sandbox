
import {ApiProperty} from '@nestjs/swagger';

import {IsNumber} from 'class-validator';


export interface ArithmeticOperators {
  a: number;
  b: number;
}

export class ArithmeticOperatorsDto implements ArithmeticOperators{
  @ApiProperty()
  @IsNumber()
  a: number;

  @ApiProperty()
  @IsNumber()
  b: number;
}