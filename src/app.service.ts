import { Injectable, Logger } from '@nestjs/common';
import { ArithmeticOperators} from './app.dtos';


@Injectable()
export class AppQueryService {

  private readonly logger = new Logger(AppQueryService.name);

  runQuery(query): string {
    this.logger.debug('will run query:'+query);
    return eval(query);
  }
}

export interface ArithmeticTemplate {
  prepareQuery(operators: ArithmeticOperators): string
}

@Injectable()
export class AdditionTemplate implements ArithmeticTemplate{
  prepareQuery(operators: ArithmeticOperators){
    return `${operators.a}+${operators.b}`;
  }
}

@Injectable()
export class SubtractionTemplate implements ArithmeticTemplate{
  prepareQuery(operators: ArithmeticOperators){
    return `${operators.a}-${operators.b}`;
  }
}

@Injectable()
export class MultiplicationTemplate implements ArithmeticTemplate{
  prepareQuery(operators: ArithmeticOperators){
    return `${operators.a}*${operators.b}`;
  }
}
