import { Controller, Logger, Post, Body } from '@nestjs/common';
import { AppQueryService,  ArithmeticTemplate, AdditionTemplate,MultiplicationTemplate,SubtractionTemplate} from './app.service';
import { ArithmeticOperators, ArithmeticOperatorsDto} from './app.dtos';

/**
 * Base Class, this one does the job. Contains "the logic"
 * But Allows for the bit and pieces to be "injected"
 */

@Controller()
class BaseController {
  private readonly logger = new Logger(BaseController.name);

  constructor(
    protected queryService: AppQueryService,
    protected arithmeticTemplate: ArithmeticTemplate
  ){}

  runOperation(operators: ArithmeticOperators){
    this.logger.debug(`Preparing a basic operation`);
    let query=this.arithmeticTemplate.prepareQuery(operators);
    this.logger.debug('the template to execute is: '+query);
    return this.queryService.runQuery(query);
  }
}

/**
 * This are the "instances", the provide no-logic in themselves
 * they are just a name + a collection of providers
 */

@Controller()
export class AdditionController extends BaseController{

  constructor(
    protected queryService: AppQueryService,
    protected arithmeticTemplate: AdditionTemplate
  ){
    super(queryService,arithmeticTemplate);
  }

  @Post('addition')
  getAddition(@Body() operators: ArithmeticOperatorsDto ): string {
    return super.runOperation(operators);
  }
}

@Controller()
export class SubtractionController extends BaseController{
  constructor(
    protected queryService: AppQueryService,
    protected arithmeticTemplate: SubtractionTemplate
  ){
    super(queryService,arithmeticTemplate);
  }

  @Post('subtraction')
  getAddition(@Body() operators: ArithmeticOperatorsDto ): string {
    return super.runOperation(operators);
  }
}

@Controller()
export class MultiplicationController extends BaseController{
  constructor(
    protected queryService: AppQueryService,
    protected arithmeticTemplate: MultiplicationTemplate
  ){
    super(queryService,arithmeticTemplate);
  }

  @Post('multiplication')
  getAddition(@Body() operators: ArithmeticOperatorsDto ): string {
    return super.runOperation(operators);
  }
}