import { Module } from '@nestjs/common';
import { AdditionController,SubtractionController, MultiplicationController } from './app.controller';
import { AppQueryService, AdditionTemplate, SubtractionTemplate, MultiplicationTemplate } from './app.service';

@Module({
  imports: [],
  controllers: [AdditionController,SubtractionController, MultiplicationController],
  providers: [AppQueryService, AdditionTemplate,SubtractionTemplate, MultiplicationTemplate],
})
export class AppModule {}
